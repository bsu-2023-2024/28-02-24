// Disables the warning about strcpy and other functions that are considered unsafe
#define _CRT_SECURE_NO_WARNINGS

// Includes the necessary libraries
#include <iostream>
#include"Strings.h"

using namespace std;

// obtainWords method: Splits the source string into words based on the specified symbols and returns an array of words
char** StringExtension::obtainWords(const char* source, int& n)
{
	char* copy = new char[strlen(source) + 1];
	strcpy(copy, source);

	char** words = new char* [strlen(copy) / 2];
	n = 0;
	const char* symbols = "	1234567890-=!@#$%^&*()_+{}|][:;'<>?/., \x22";
	char* pword = strtok(copy, symbols);
	words[n] = new char[strlen(pword) + 1];
	strcpy(words[n], pword);

	n++;
	while (pword)
	{
		pword = strtok(nullptr, symbols);
		if (pword)
		{
			words[n] = new char[strlen(pword) + 1];
			strcpy(words[n], pword);
			n++;
		}
	}

	delete[] copy;

	char** result = new char* [n];

	for (int i = 0; i < n; i++)
	{
		result[i] = words[i];
	}

	delete[]words;
	cout << result << endl;
	return result;
}

// obtainWordsAgain method: Similar to obtainWords, but uses a different set of symbols for splitting the source string
char** StringExtension::obtainWordsAgain(char* source, int& n)
{
	const char* symbols = "ABCDEFGHIJKLMNOPQRSTUWVXYZabcdefghijklnmopqrstuwvxyz";
	char** words = new char* [strlen(source) / 2];
	n = 0;
	char* pword = strpbrk(source, symbols);
	while (pword)
	{
		int length = strspn(pword, symbols);
		words[n] = new char[length + 1];
		strncpy(words[n], pword, length);
		words[n][length] = '\0';
		pword = strpbrk(pword + length, symbols);
		n++;
	}

	char** result = new char* [n];

	for (int i = 0; i < n; i++)
	{
		result[i] = words[i];
	}

	delete[]words;
	return result;
}

// displayWords method: Prints the words in the given array to the console
void StringExtension::displayWords(char** words, int n)
{
	for (int i = 0; i < n; i++)
	{
		std::cout << (i + 1) << ". " << words[i] << std::endl;
	}
}

// freeHeap method: Frees the memory allocated for the array of words
void StringExtension::freeHeap(char** words, int n)
{
	for (int i = 0; i < n; i++)
	{
		delete[] words[i];
	}

	delete[] words;
}

// sortingWords method: Sorts the array of words in ascending order
void StringExtension::sortingWords(char** words, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = n - 1; j > i; j--)
		{
			if (strcmp(words[j], words[j - 1]) < 0)
			{
				StringExtension::swap(words[j], words[j - 1]);
			}
		}
	}
}

// swap method: Swaps the positions of two words in the array
void StringExtension::swap(char*& x, char*& y)
{
	char* temp = x;
	x = y;
	y = temp;
}

// replaceSubString method: Replaces all occurrences of a substring in a string with another string
void StringExtension::replaceSubString(char* string, char* subString, char* replaceString)
{
	int lenSubString = strlen(subString), lenReplaceString = strlen(replaceString);

	char* pString = strstr(string, subString);

	char* help = new char[strlen(string) + 1];

	while (pString)
	{
		strcpy(help, pString + lenSubString);
		strcpy(pString, replaceString);
		strcat(pString, help);
		pString = strstr(pString + lenReplaceString, subString);
	}

	delete[] help;
}

// deleteSubString method: Deletes all occurrences of a substring in a string
void StringExtension::deleteSubString(char* string, char* subString)
{
	int length = strlen(subString);

	char* pString = strstr(string, subString);

	while (pString)
	{
		strcpy(pString, pString + length);
		pString = strstr(pString, subString);
	}
}

// deleteWords method: Deletes all words of a certain length from the source string
void StringExtension::deleteSubString(char* source, int length)
{
	const char* symbols = "ABCDEFGHIJKLMNOPQRSTUWVXYZabcdefghijklnmopqrstuwvxyz";
	char* pword = strpbrk(source, symbols);
	while (pword)
	{
		int n = strspn(pword, symbols);

		if (length == n)
		{
			strcpy(pword, pword + n);
		}

		pword = strpbrk(pword, symbols);
	}
}